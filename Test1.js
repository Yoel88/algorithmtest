// - `anagram('aaz', 'zza')` => false
//- `anagram('anagram', 'nagaram'))` => true

 
   //Cek dahulu apakah jumlah karakternya sama
   //Cek apakah karakter yang digunakan sama
   //Jika jumlah karakter dan karakter yang digunakan sama maka true
   //Jika jumlah karakter dan karakter yang digunakan sama maka false
   //Jika keduanya true, cek apakah karakter yang digunakan
   //Jika true maka sudah sesuai


  function anagram(a1, a2){
  if (a1.length !== a2.length){
    return false
  }
  if (a1 === a2){
    return true
  }

  var a = '',
  i = 0,
  limit = a1.length,
  match = 0,
  index;

  while (i < a1.length) {
    a = a1.substr(i++, 1)
    index = a2.indexOf(a)
    if (index > -1){
      match ++
      a2 = a2.substr(0, index)+ a2.substr(index+1)
    }else {
      return false;
    }
  }
  return match === a1.length
}

console.log(anagram('aaz', 'zza'))
console.log(anagram('anagram', 'nagaram'))