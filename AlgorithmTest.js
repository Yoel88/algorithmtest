
    // -   `countUniqueValues([1, 2, 3, 4, 4, 4, 7, 7, 12, 12, 13]))` => 7
    // -   `countUniqueValues([1, 2, 2, 2, 2, 2, 2, 2, 4, 6]))` => 4
    // -   `countUniqueValues([]))` => 0

    function countUniqueValues (num){
    return new Set (num).size;
    }

    console.log(countUniqueValues([1, 2, 3, 4, 4, 4, 7, 7, 12, 12, 13]));
    console.log(countUniqueValues([1, 2, 2, 2, 2, 2, 2, 2, 4, 6]));
    console.log(countUniqueValues([]));

    // function countUniqueValues (num){
    //     var count = 1;
    //     var result = "";
    //     for (var i = 0; i<num.length; i++){
    //         if(num[i]==num[i+1]){
    //             count +=1;
    //         }
    //         else{
    //             result += num[i] + count;
    //             count = 1
    //         }
    //     }
    // }
    // console.log(countUniqueValues([1, 2, 3, 4, 4, 4, 7, 7, 12, 12, 13]));
    // console.log(countUniqueValues([1, 2, 2, 2, 2, 2, 2, 2, 4, 6]));
    // console.log(countUniqueValues([]));